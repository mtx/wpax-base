<?php

/**
* Plugin Name: WPax Utilities
* Description: This plugin contains all of my awesome custom functions.
* Author: Alex Popescu
* Version: 1.3
*/

/*
- anti spam bots [email]
- admin toolbelt
- 29-12-15 remove pings to self
- 29-12-15 Add a Post’s Category Name to Body Class

- functions:
	- getUserRole

*/



// - - - - - - - - - - - - - - - - - - - - -
// Hide Email from Spam Bots using a short code place this in your functions file
// http://codex.wordpress.org/Function_Reference/antispambot
//- - - - - - - - - - - - - - - - - - - - -

function HideMail($atts , $content = null ){
	if ( ! is_email ($content) )
		return;

	return '<a href="mailto:'.antispambot($content).'">'.antispambot($content).'</a>';
}
add_shortcode( 'email','HideMail');


// - - - - - - - - - - - - - - - - - - - - -
// admin Toolbelt
//- - - - - - - - - - - - - - - - - - - - -

function getUserRole($userID){
$user = new WP_User( $userID );
if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
	foreach ( $user->roles as $role )
		return $role;
	}
}


function addToFooter() {
	global $current_user;
	if ( is_user_logged_in() && (getUserRole($current_user->ID)=='administrator')) {
		global $pagename;
		global $template;

		if (is_single () || is_page() && !is_front_page()) $edit=' <li><a href="'.get_edit_post_link().'"><span>&#x270f;</span>  EDIT</a>  '.get_the_ID().'</li>';
			else $edit='';

		if ($pagename) $zePagename = '<li>pagename: <b>'.$pagename.'</b></li>';
			else $zePagename='';
	echo '
		<style type="text/css">
		#toolBelt, #toolBelt * {font-size: 12px; }
		#toolBelt {position: fixed; bottom: 0; left: 0; padding: 0.5ex 1ex; font-size: 1em;  background-color: Lavender; color: #333; line-height: 1.2; z-index: 1001; opacity: 0.9; width: auto;  margin: 0;  box-shadow: 0 0 1ex #666}
		#toolBelt li a {color: BlueViolet;}
		#toolBelt li {padding: 0.5ex 0 0.5ex 1.5ex ; display: inline-block; margin: 0 1ex;  border-left: 1px dotted #CCC }
		#toolBelt li span {display: inline-block; margin-right: 1ex}
		#toolBelt li:first-child {border: none}
		#toolBelt:hover {opacity: 1; }
		</style>
		<ul id="toolBelt" class="lyst">
			<li><span>&#128204;</span>  Hello, <strong>'.$current_user->user_login.'</strong>!</li>
			<li>template: <b>'.basename($template).'</b> </li>
			'.$zePagename.'
			<li> '.get_num_queries().' queries in '.timer_stop().'s</li>
			<li><a class="underline" href="'.admin_url().'"><span>&#128295;</span> Admin</a> </li>
			'.$edit.'
			<li> <a class="" href="'.wp_logout_url(home_url( '' )).'">Logout</a></li>
		</ul>
	';
	}
}
add_action( 'wp_footer', 'addToFooter' );

// - - - - - - - - - - - - - - - - - - - - -
// remove pings to self
//- - - - - - - - - - - - - - - - - - - - -

	function no_self_ping( &$links ) {
			$home = get_option( 'home' );
			foreach ( $links as $l => $link )
					if ( 0 === strpos( $link, $home ) )
							unset($links[$l]);
	}
	add_action( 'pre_ping', 'no_self_ping' );

// - - - - - - - - - - - - - - - - - - - - -
// Add a Post’s Category Name to Body Class
// http://bavotasan.com/2011/add-a-posts-category-name-to-body-class-in-wordpress/
//- - - - - - - - - - - - - - - - - - - - -

	function add_category_name($classes = '') {
		 if(is_single()) {
				$category = get_the_category();
				$classes[] = 'category-'.$category[0]->slug;
		 }
		 return $classes;
	}
	add_filter('body_class','add_category_name');

?>